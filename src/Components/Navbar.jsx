import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Navbar.css';
import Favicon from './../Images/favicon.png';



class Navbar extends Component {
	render() {
		return (
			<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
				<div className='container'>
					<Link className="navbar-brand" to="/">
						<img src={Favicon} />
					</Link>
					<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
						<span className="navbar-toggler-icon"></span>
					</button>

					<div className="collapse navbar-collapse" id="navbarText">
						<ul className="navbar-nav ml-auto">
							<li className="nav-item">
								<Link className="nav-link" to="/">Pagrindinis <span className="sr-only">(current)</span></Link>
							</li>
							<li className="nav-item">
								<Link className="nav-link" to="/portfolio">Portfolio</Link>
							</li>
							<li className="nav-item">
								<Link className="nav-link" to="/about">Apie</Link>
							</li>
							<li className="nav-item">
								<Link className="nav-link" to="/contact">Susisiekite</Link>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		)
	}
}

export default Navbar