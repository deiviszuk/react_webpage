import React, { Component } from 'react';
import Navbar from '../Components/Navbar';
import Footer from '../Components/Footer';
import Logo from './../Images/logo.png';

class Home extends Component {
	render() {
		return (
			<div className="home-background">
				<Navbar />
				<div className="container home">
				<img src={Logo} className="logo"/>
					<h1 className="home-header">
						3D VIZUALIZACIJOS
					</h1>
					<h2 className="home-h2 ">
						Atliekame įvairiausio sudėtingumo 3D vizualizacijas. Nuo namų interjero iki namų kvartalų.
					</h2>
				</div>
			</div>
		);
	}
}

export default Home;