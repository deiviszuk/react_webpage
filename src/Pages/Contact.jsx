import React, { Component } from 'react';
import Navbar from '../Components/Navbar';
import Footer from '../Components/Footer';


class Contact extends Component {
	render() {
		return (
			<div className="contact-background">
				<Navbar />
				<div className="col-md-6 offset-md-3 contact-form container">
				<h1> Susisiekite su mumis:</h1>
					<div className="card card-outline-secondary">
						<div className="card-body">
							<form className="form" role="form" autocomplete="off">
								<fieldset>
									<label for="name2" className="mb-0">Vardas</label>
									<div className="row mb-1">
										<div className="col-lg-12">
											<input type="text" name="name2" id="name2" className="form-control" required="" />
										</div>
									</div>
									<label for="email2" className="mb-0">El.Paštas</label>
									<div className="row mb-1">
										<div className="col-lg-12">
											<input type="text" name="email2" id="email2" className="form-control" required="" />
										</div>
									</div>
									<label for="message2" className="mb-0">Žinutė</label>
									<div className="row mb-1">
										<div className="col-lg-12">
											<textarea rows="6" name="message2" id="message2" className="form-control" required=""></textarea>
										</div>
									</div>
									<button type="submit" className="btn btn-secondary btn-lg float-right">Siųsti!</button>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Contact;