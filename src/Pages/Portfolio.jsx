import React, { Component } from 'react';
import Navbar from '../Components/Navbar';
import Footer from '../Components/Footer';
import Gallery from 'react-photo-gallery';

const PHOTO_SET = [
	{
		src: '../portfolio/1-min.jpg',
		width: 5,
		height: 3
	},
	{
		src: '../portfolio/2-min.jpg',
		width: 5,
		height: 3
	},
	{
		src: '../portfolio/3-min.jpg',
		width: 5,
		height: 3
	},
	{
		src: '../portfolio/4-min.jpg',
		width: 5,
		height: 3
	},
	{
		src: '../portfolio/5-min.jpg',
		width: 5,
		height: 3
	},
	{
		src: '../portfolio/6-min.jpg',
		width: 5,
		height: 3
	},
	{
		src: '../portfolio/7-min.jpg',
		width: 5,
		height: 3
	},
	{
		src: '../portfolio/8-min.jpg',
		width: 5,
		height: 3
	},
	{
		src: '../portfolio/9-min.jpg',
		width: 5,
		height: 3
	},
	{
		src: '../portfolio/10-min.jpg',
		width: 5,
		height: 3
	},
	{
		src: '../portfolio/11-min.jpg',
		width: 5,
		height: 3
	},
	{
		src: '../portfolio/12-min.jpg',
		width: 5,
		height: 3
	}
];

class Portfolio extends Component {
	render() {
		return (
			<div className="portfolio-background">
				<Navbar />
				<div className="foto-gallery container-fluid">
					<Gallery photos={PHOTO_SET} />
				</div>
			</div>
		);
	}
}

export default Portfolio;