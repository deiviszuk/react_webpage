import React, { Component } from 'react';
import Navbar from '../Components/Navbar';
import Footer from '../Components/Footer';


class About extends Component {
	render() {
		return (
			<div className="about-background">
				<Navbar />
				<br />
				<div className="container">
				<h1>Mus rasite:</h1>
					<div className="row-fluid">
						<div className="span8">
						<iframe width="100%" height="350" id="gmap_canvas" src="https://maps.google.com/maps?q=verkiu%2030b%20&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
						</div>

						<div className="span4">
							<h2>Biuro adresas</h2>
							<address>
								<strong>Vilnius</strong><br />
								<strong>Verkiu 30b</strong><br />
								<strong>Telefonas: 860777390</strong><br />
								<strong>El.Paštas: archi.vizual@gmail.com</strong><br />
							</address>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default About;