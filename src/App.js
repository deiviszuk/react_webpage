import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import Home from './Pages/Home';
import About from './Pages/About';
import Contact from './Pages/Contact';
import Portfolio from './Pages/Portfolio';
import Footer from './Components/Footer';

class App extends Component {
  render() {
    return (
      <div>
      <Router>
           <div>
              <Route exact path="/" component={Home}/>
              <Route exact path="/about" component={About}/>
              <Route exact path="/contact" component={Contact}/>
              <Route exact path="/portfolio" component={Portfolio}/>
           </div>
      </Router>
      </div>
    )
  }
}

export default App;
